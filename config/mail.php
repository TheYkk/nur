<?php 

return [
  'driver' => 'smtp',
  'host' => 'smtp.localhost',
  'port' => 587,
  'username' => 'root',
  'password' => '',
  'encryption' => 'tls',
  'charset' => 'utf8',
  'from' => [
    'address' => 'hello@localhost',
    'name' => 'Example',
  ],
];
